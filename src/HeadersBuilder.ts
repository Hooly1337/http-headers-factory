/*let pos = 1;
const headersOrder: Record<string, number> = {
    Host: pos++,
    Connection: pos++,
    'Cache-Control': pos++,
    'Upgrade-Insecure-Requests': pos++,
    'User-Agent': pos++,
    'Content-Type': pos++,
    'Accept': pos++,
    'Origin': pos++,
    'Sec-Fetch-Site': pos++,
    'Sec-Fetch-Mode': pos++,
    'Sec-Fetch-User': pos++,
    'Sec-Fetch-Dest': pos++,
    'Referer': pos++,
    'Accept-Encoding': pos++,
    'Accept-Language': pos++,
    'If-None-Match': pos++,
    'Cookie': Infinity // Cookie is always last
};*/


export type HeadersKeys = 'Host' | 'Connection' | 'Cache-Control' | 'Upgrade-Insecure-Requests' | 'User-Agent' |
    'Content-Type' | 'Accept' | 'Origin' | 'Sec-Fetch-Site' | 'Sec-Fetch-Mode' | 'Sec-Fetch-User' | 'Sec-Fetch-Dest' |
    'Referer' | 'Accept-Encoding' | 'Accept-Language' | 'If-None-Match' | 'Cookie' | 'Content-Length';

export type StrictHeaders = Partial<Record<HeadersKeys, string>>


interface IHeadersBuilder {
    setUserAgent(user_agent: string): this;
    setConnection(connection: string): this;
    setAccept(accept: string): this;
    setUpgradeInsecureRequests(option: 0|1): this;
    setSecFetchSite(option: string): this;
    setSecFetchMode(option: string): this;
    setSecFetchUser(option: string): this;
    setSecFetchDest(option: string): this;
    setCacheControl(cache_control: string): this;
    setAcceptEncoding(encoding: string): this;
    setAcceptLanguage(language: string): this;
    setLang(lang: string): this;
    setContentType(type: string): this;
    setOrigin(origin: string): this;
    setReferer(referer: string): this;
    setCookie(cookie: string): this;
    setHost(host: string): this;
    add(key: string, value: string): this;
    setIfNoneMatch(option: string): this;
    setContentLength(length: number): this;
}

export class HeadersBuilder implements IHeadersBuilder {
    protected headers: StrictHeaders = {};
    protected customHeaders: Record<string, string> = {};

    constructor() {}

    public reset(): void {
        this.headers = {};
        this.customHeaders = {}
    }

    public setContentLength(length: number) {
        this.headers['Content-Length'] = `${length}`;
        return this
    }

    public setUpgradeInsecureRequests(option: 0|1): this {
        this.headers['Upgrade-Insecure-Requests'] = `${option}`;
        return this;
    }

    public setAcceptLanguage(language: string): this {
        this.headers['Accept-Language'] = language;
        return this;
    }

    public setAcceptEncoding(encoding: string): this {
        this.headers['Accept-Encoding'] = encoding;
        return this;
    }

    public setCacheControl(cache_control: string): this {
        this.headers['Cache-Control'] = cache_control;
        return this
    }

    public setSecFetchDest(option: string): this {
        this.headers['Sec-Fetch-Dest'] = option;
        return this;
    }

    public setSecFetchUser(option: string): this {
        this.headers['Sec-Fetch-User'] = option;
        return this;
    }

    public setSecFetchMode(option: string): this {
        this.headers['Sec-Fetch-Mode'] = option;
        return this;
    }

    public setSecFetchSite(option: string): this {
        this.headers['Sec-Fetch-Site'] = option;
        return this
    }

    public setAccept(accept: string): this {
        this.headers['Accept'] = accept;
        return this
    }

    public setConnection(connection: string): this {
        this.headers['Connection'] = connection;
        return this
    }

    public setUserAgent(user_agent: string): this {
        this.headers['User-Agent'] = user_agent;
        return this
    }

    public setHost(host: string): this {
        this.headers['Host'] = host;
        return this
    }

    public setLang(lang: string): this {
        this.headers['Accept-Language'] = lang;
        return this
    }

    public setContentType(type: string): this {
        this.headers['Content-Type'] = type;
        return this
    }

    public setOrigin(origin: string): this {
        this.headers['Origin'] = origin;
        return this
    }

    public setReferer(referer: string): this {
        this.headers['Referer'] = referer;
        return this
    }

    public setCookie(cookie: string): this {
        this.headers['Cookie'] = cookie;
        return this
    }

    public setIfNoneMatch(option: string): this{
        this.headers['If-None-Match'] = option;
        return this;
    }

    public add(key: string, value: string): this{
        this.customHeaders[key] = value;
        return this
    }



    public build(): Record<string, string> {
        let result: Record<string, string> = {};
        if (this.headers.Host) {
            result['Host'] = this.headers.Host
        }

        if (this.headers.Connection) {
            result['Connection'] = this.headers.Connection
        }

        if (this.headers['Content-Length']) {
            result['Content-Length'] = this.headers["Content-Length"]
        }

        if (this.headers["Cache-Control"]) {
            result["Cache-Control"] = this.headers["Cache-Control"]
        }

        if (this.headers["Upgrade-Insecure-Requests"]) {
            result["Upgrade-Insecure-Requests"] = this.headers["Upgrade-Insecure-Requests"]
        }

        if (this.headers["User-Agent"]) {
            result["User-Agent"] = this.headers["User-Agent"]
        }

        if (this.headers["Content-Type"]) {
            result['Content-Type'] = this.headers["Content-Type"]
        }

        if (this.headers.Accept) {
            result['Accept'] = this.headers.Accept
        }

        if (this.headers.Origin) {
            result['Origin'] = this.headers.Origin
        }

        if (this.headers["Sec-Fetch-Site"]) {
            result["Sec-Fetch-Site"] = this.headers["Sec-Fetch-Site"]
        }

        if (this.headers["Sec-Fetch-Mode"]) {
            result["Sec-Fetch-Mode"] = this.headers["Sec-Fetch-Mode"]
        }

        if (this.headers["Sec-Fetch-User"]) {
            result["Sec-Fetch-User"] = this.headers["Sec-Fetch-User"]
        }

        if (this.headers["Sec-Fetch-Dest"]) {
            result["Sec-Fetch-Dest"] = this.headers["Sec-Fetch-Dest"]
        }

        if (this.headers.Referer) {
            result['Referer'] = this.headers.Referer
        }

        if (this.headers["Accept-Encoding"]) {
            result["Accept-Encoding"] = this.headers["Accept-Encoding"]
        }

        if (this.headers["Accept-Language"]) {
            result["Accept-Language"] = this.headers["Accept-Language"]
        }

        if (this.headers["If-None-Match"]) {
            result["If-None-Match"] = this.headers["If-None-Match"]
        }

        if( Object.entries(this.customHeaders).length !== 0) {
            for (let header in this.customHeaders) {
                result[header] = this.customHeaders[header]
            }
        }

        if (this.headers.Cookie) {
            result['Cookie'] = this.headers.Cookie
        }



        this.reset();
        return result;
    }
}

