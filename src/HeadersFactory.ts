import {HeadersBuilder, StrictHeaders} from './HeadersBuilder'




interface IHeadersFactory {
    doc(options: BaseOptions): StrictHeaders,
    fetch(options: FetchOptions): StrictHeaders,
    img(options: BaseOptions): StrictHeaders,
    css(options: BaseOptions): StrictHeaders,
    js(options: BaseOptions): StrictHeaders,
}

interface IHeadersSettings {
    lang: string,
    userAgent: string,
    host: string,
}

type BaseOptions = {
    referer?: string,
    cookie?: string,
    ifNoneMatch?: string
}

type FetchOptions = BaseOptions & {
    extraHeaders?: Record<string, string>,
    contentType?: string,
    origin?: string,
    contentLength?: number
}


export class HeadersFactory implements IHeadersFactory{

    constructor(protected readonly settings: IHeadersSettings) {}


    public doc(options: BaseOptions): StrictHeaders {
        let builder = new HeadersBuilder().
        setHost(this.settings.host).
        setConnection('keep-alive').setCacheControl('max-age=0').
        setUpgradeInsecureRequests(1).setUserAgent(this.settings.userAgent).
        setAccept('text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,' +
                  'application/signed-exchange;v=b3;q=0.9').
        setSecFetchSite('none').
        setSecFetchMode('navigate').
        setSecFetchUser('?1').
        setSecFetchDest('document').
        setAcceptEncoding('gzip, deflate, br').
        setAcceptLanguage(this.settings.lang);

        if (options.ifNoneMatch) {
            builder.setIfNoneMatch(options.ifNoneMatch)
        }

        if (options.cookie) {
            builder.setCookie(options.cookie)
        }

        if (options.referer) {
            builder.setReferer(options.referer)
        }

        return builder.build()
    }

    public css(options: BaseOptions): StrictHeaders {
        let builder = new HeadersBuilder().
            setHost(this.settings.host).
            setConnection('keep-alive').
            setUserAgent(this.settings.userAgent).
            setAccept('text/css,*/*;q=0.1').
            setSecFetchSite('same-origin').
            setSecFetchMode('no-cors').
            setSecFetchDest('style').
            setAcceptEncoding('gzip, deflate, br').
            setAcceptLanguage(this.settings.lang);

        if (options.ifNoneMatch) {
            builder.setIfNoneMatch(options.ifNoneMatch)
        }

        if (options.cookie) {
            builder.setCookie(options.cookie)
        }

        if (options.referer) {
            builder.setReferer(options.referer)
        }

        return builder.build()
    }

    public js(options: BaseOptions): StrictHeaders {
        let builder = new HeadersBuilder().
            setHost(this.settings.host).
            setConnection('keep-alive').
            setUserAgent(this.settings.userAgent).
            setAccept('*/*').
            setSecFetchSite('same-origin').
            setSecFetchMode('no-cors').
            setSecFetchDest('script').
            setAcceptEncoding('gzip, deflate, br').
            setAcceptLanguage(this.settings.lang);

        if (options.referer) {
            builder.setReferer(options.referer)
        }

        if (options.cookie) {
            builder.setCookie(options.cookie)
        }

        // js requests haven't If-None-Match header

        return builder.build()
    }

    public img(options: BaseOptions): StrictHeaders {
        let builder = new HeadersBuilder().
            setHost(this.settings.host).
            setConnection('keep-alive').
            setUserAgent(this.settings.userAgent).
            setAccept('image/avif,image/webp,image/apng,image/*,*/*;q=0.8').
            setSecFetchSite('same-origin').
            setSecFetchMode('no-cors').
            setSecFetchDest('image').
            setAcceptEncoding('gzip, deflate, br').
            setAcceptLanguage(this.settings.lang);

        if (options.cookie) {
            builder.setCookie(options.cookie)
        }

        if (options.referer) {
            builder.setReferer(options.referer)
        }

        // img requests haven't If-None-Match header

        return builder.build()
    }

    public fetch(options: FetchOptions): StrictHeaders {
        let builder = new HeadersBuilder().
            setHost(this.settings.host).
            setConnection('keep-alive').
            setUserAgent(this.settings.userAgent).
            setAccept('*/*').
            setSecFetchSite('same-origin').
            setSecFetchMode('cors').
            setSecFetchDest('empty').
            setAcceptEncoding('gzip, deflate, br').
            setAcceptLanguage(this.settings.lang);

        if (options.cookie) {
            builder.setCookie(options.cookie)
        }

        if (options.referer) {
            builder.setReferer(options.referer)
        }

        if (options.origin) {
            builder.setOrigin(options.origin)
        }

        if (options.contentType) {
            builder.setContentType(options.contentType)
        }

        if (options.contentLength) {
            builder.setContentLength(options.contentLength)
        }

        if (options.extraHeaders) {
            for (let header in options.extraHeaders) {
                builder.add(header, options.extraHeaders[header])
            }
        }



        return builder.build()
    }


}
