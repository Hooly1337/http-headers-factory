import {HeadersFactory} from "../src/HeadersFactory";

describe("headers factory.fetch test suite", () =>
{
    it("Should normalize headers correctly", () =>
    {
        expect(JSON.stringify(new HeadersFactory({lang: 'lang', userAgent: 'user-agent', host: 'www.example.com'}).
            fetch({extraHeaders: {'Extra-Header-1': 'test', 'Extra-Header-2': 'test'}, cookie:'foo=bar',
            origin: 'www.example.com', referer: 'www.example.com/index.html', contentType: 'content-type',
            ifNoneMatch: 'some hash', contentLength: 1337})
        )).toBe(JSON.stringify({
            Host: "www.example.com",
            Connection: "keep-alive",
            'Content-Length': '1337',
            "User-Agent": "user-agent",
            'Content-Type': 'content-type',
            Accept: "*/*",
            Origin: 'www.example.com',
            'Sec-Fetch-Site': 'same-origin',
            'Sec-Fetch-Mode': 'cors',
            'Sec-Fetch-Dest': 'empty',
            Referer: "www.example.com/index.html",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "lang",
            "Extra-Header-1": "test",
            "Extra-Header-2": "test",
            Cookie: "foo=bar",
        }));
    })
});