import {HeadersBuilder} from "../src/HeadersBuilder";

describe("headers builder test suite", () =>
{
    it("Should normalize headers correctly", () =>
    {
        expect(JSON.stringify(new HeadersBuilder().
            add('Extra-Header-1', 'test').
            add('Extra-Header-2', 'test').
            setConnection('keep-alive').
            setCookie('foo=bar').
            setUpgradeInsecureRequests(1).
            setUserAgent("<USER_AGENT>").
            setAccept('*/*').
            setReferer('https://www.google.com/').
            setAcceptEncoding('gzip, deflate, br').
            setAcceptLanguage('ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7').
            setHost('www.example.com').
            setReferer('https://www.google.com/').
            build()
        )).toBe(JSON.stringify({
            Host: "www.example.com",
            Connection: "keep-alive",
            "Upgrade-Insecure-Requests": "1",
            "User-Agent": "<USER_AGENT>",
            Accept: "*/*",
            Referer: "https://www.google.com/",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
            "Extra-Header-1": "test",
            "Extra-Header-2": "test",
            Cookie: "foo=bar",
        }));
    })
});